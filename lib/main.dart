import 'package:doctor_plus/dependency_injector.dart';
import 'package:flutter/material.dart';

import 'app/my_app.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  injectDependecies();
  runApp(const MyApp());
}
