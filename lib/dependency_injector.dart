import 'package:dio/dio.dart';
import 'package:doctor_plus/app/data/datasource/remote/authentication_api.dart';
import 'package:doctor_plus/app/data/datasource/repository_impl/authentication_respository_impl.dart';
import 'package:doctor_plus/app/domain/repository/authentication_respository.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

void injectDependecies() {
  //Create Dio var
  Dio _dio = Dio(
    BaseOptions(baseUrl: 'https://8ca8-191-100-27-218.ngrok.io/api/'),
  );
//Inject  dio
  Get.i.lazyPut<Dio>(() => _dio);

// Inject AuthenticationRepository
  Get.i.lazyPut<AuthenticationRepository>(
      () => AuthenticationRepositoryImpl(AuthenticationAPI()));

  final _storage = new FlutterSecureStorage();
  Get.i.lazyPut<FlutterSecureStorage>(() => _storage);
}
