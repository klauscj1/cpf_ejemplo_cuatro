import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OptionModel {
  final int id;
  final String text;
  final IconData icon;

  OptionModel({
    required this.id,
    required this.text,
    required this.icon,
  });
}

List<OptionModel> options = [
  OptionModel(
    id: 1,
    text: "As a free health question",
    icon: Icons.message_outlined,
  ),
  OptionModel(
    id: 2,
    text: "Next consult for you",
    icon: Icons.format_list_numbered_outlined,
  ),
  OptionModel(
    id: 3,
    text: "Health feed",
    icon: Icons.list_alt_sharp,
  ),
  OptionModel(
    id: 4,
    text: "Online Consults",
    icon: Icons.mobile_screen_share_outlined,
  ),
];
