class Country {
  final String name;
  final String flagUrl;
  final String code;
  final int id;
  final int codePhone;

  Country({
    required this.name,
    required this.flagUrl,
    required this.code,
    required this.id,
    required this.codePhone,
  });
}

List<Country> countries = [
  Country(
      name: 'Ecuador',
      flagUrl: 'https://flagcdn.com/w20/ec.png',
      code: 'EC',
      id: 1,
      codePhone: 593),
  Country(
      name: 'Argentina',
      flagUrl: 'https://flagcdn.com/w20/ar.png',
      code: 'AR',
      id: 2,
      codePhone: 456),
  Country(
      name: 'Brasil',
      flagUrl: 'https://flagcdn.com/w20/br.png',
      code: 'BR',
      id: 3,
      codePhone: 3),
];
