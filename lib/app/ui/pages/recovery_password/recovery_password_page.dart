import 'package:doctor_plus/app/ui/pages/recovery_password/controller/recovery_password_controller.dart';
import 'package:doctor_plus/app/ui/pages/success/sucess_page.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:doctor_plus/app/ui/widgets/custom_back_button.dart';
import 'package:doctor_plus/app/ui/widgets/custom_button.dart';
import 'package:doctor_plus/app/ui/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_meedu/meedu.dart';

final recoveryPasswordProvider = SimpleProvider(
  (ref) => RecoveryPasswordController(),
);

class RecoveryPasswordPage extends StatelessWidget {
  const RecoveryPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              CustomBackButton(
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "Recovery Password",
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Colors.black),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "Reset code was send to you email. Please enter the\ncode and create a new password",
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                    ),
              ),
              const SizedBox(
                height: 30,
              ),
              const Text("Reset Code"),
              const SizedBox(
                height: 5,
              ),
              Consumer(
                builder: (_, ref, __) {
                  final isValid = ref
                      .watch(recoveryPasswordProvider.ids(() => ['codeValid']))
                      .codeValid;
                  return CustomTextField(
                    isValid: isValid,
                    enableIcon: Icons.check_circle,
                    disableIcon: Icons.close,
                    controller:
                        recoveryPasswordProvider.read.emailTextController,
                    onChanged: recoveryPasswordProvider.read.onChangeCode,
                    onPressed: () {},
                  );
                },
              ),
              const SizedBox(
                height: 20,
              ),
              const Text("Password"),
              const SizedBox(
                height: 5,
              ),
              Consumer(
                builder: (_, ref, __) {
                  final showPassword = ref
                      .watch(
                          recoveryPasswordProvider.ids(() => ['showPassword']))
                      .showPassword;
                  return CustomTextField(
                    isPassword: true,
                    isValid: showPassword,
                    enableIcon: Icons.visibility_off,
                    disableIcon: Icons.visibility,
                    controller:
                        recoveryPasswordProvider.read.passwordTextController,
                    onChanged: (value) {},
                    onPressed: recoveryPasswordProvider.read.changeShowPassword,
                  );
                },
              ),
              const SizedBox(
                height: 20,
              ),
              const Text("Confirm your password"),
              const SizedBox(
                height: 5,
              ),
              Consumer(
                builder: (_, ref, __) {
                  final showPassword = ref
                      .watch(
                          recoveryPasswordProvider.ids(() => ['showPassword2']))
                      .showPassword2;
                  return CustomTextField(
                    isPassword: true,
                    isValid: showPassword,
                    enableIcon: Icons.visibility_off,
                    disableIcon: Icons.visibility,
                    controller:
                        recoveryPasswordProvider.read.passwordTextController2,
                    onChanged: (value) {},
                    onPressed:
                        recoveryPasswordProvider.read.changeShowPassword2,
                  );
                },
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * .07,
                child: CustomButton(
                  label: "Change Password",
                  onTap: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (_) => const SuccessPage(
                          routeName: Routes.login,
                          description:
                              "You have successfully change password.\nPlease use your new password when logging in",
                          textButton: "Log In Now",
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
