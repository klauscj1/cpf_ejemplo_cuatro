import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

class RecoveryPasswordController extends SimpleNotifier {
  //variables
  bool _codeValid = false;
  bool _showPassword = false;
  bool _showPassword2 = false;
  TextEditingController emailTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  TextEditingController passwordTextController2 = TextEditingController();

  //get's
  bool get codeValid => _codeValid;
  bool get showPassword => _showPassword;
  bool get showPassword2 => _showPassword2;

  //set's
  set codeValid(bool value) {
    _codeValid = value;
    notify(['codeValid']);
  }

  set showPassword(bool value) {
    _showPassword = value;
    notify(['showPassword']);
  }

  set showPassword2(bool value) {
    _showPassword2 = value;
    notify(['showPassword2']);
  }
  //methods

  void onChangeCode(String value) {
    if (value.length > 4) {
      _codeValid = true;
    } else {
      _codeValid = false;
    }
    notify(['codeValid']);
  }

  void changeShowPassword() {
    _showPassword = !_showPassword;
    notify(['showPassword']);
  }

  void changeShowPassword2() {
    _showPassword2 = !_showPassword2;
    notify(['showPassword2']);
  }
}
