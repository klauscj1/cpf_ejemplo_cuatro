import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SplashController extends SimpleNotifier {
  final _storage = Get.i.find<FlutterSecureStorage>();
  String route = "";
  SplashController() {
    _init();
  }

  String text = 'Loading';
  bool navigate = false;
  void _init() async {
    String? token = await _storage.read(key: 'token');
    await await Future.delayed(const Duration(milliseconds: 500));
    text = text + ".";
    notify();
    await Future.delayed(const Duration(milliseconds: 500));
    text = text + ".";
    notify();
    await Future.delayed(const Duration(milliseconds: 500));
    text = text + ".";
    await Future.delayed(const Duration(milliseconds: 200));
    if (token != null && token.isNotEmpty) {
      route = Routes.home;
    } else {
      route = Routes.onboarding;
    }
    navigate = true;
    notify();
  }
}
