import 'package:doctor_plus/app/ui/pages/splash/controller/splash_controller.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_meedu/state.dart';

final splashProvider = SimpleProvider(
  (ref) => SplashController(),
);

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ProviderListener<SplashController>(
      provider: splashProvider,
      onChange: (context, controller) {
        if (controller.navigate) {
          Navigator.pushReplacementNamed(context, splashProvider.read.route);
        }
      },
      builder: (_, controller) {
        return Scaffold(
          body: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/logo.png',
                width: MediaQuery.of(context).size.width * .33,
              ),
              Consumer(
                builder: (_, ref, __) {
                  final splashController = ref.watch(splashProvider);
                  return Text(
                    splashController.text,
                    style: Theme.of(context).textTheme.overline,
                  );
                },
              )
            ],
          )),
        );
      },
    );
  }
}
