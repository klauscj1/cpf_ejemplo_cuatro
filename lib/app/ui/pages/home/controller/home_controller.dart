import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class HomeController extends SimpleNotifier {
  final _storage = Get.i.find<FlutterSecureStorage>();
  //var's
  int _index = 0;
//get's
  int get index => _index;
//set's
  set index(int value) {
    _index = value;
    notify(['index']);
  }

//method's
  void changeIndex(int value) {
    _index = value;
    notify(['index']);
  }

  Future<void> logout() async {
    await _storage.delete(key: 'token');
  }
}
