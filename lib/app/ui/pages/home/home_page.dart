import 'package:doctor_plus/app/ui/pages/home/controller/home_controller.dart';

import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

import 'widgets/custom_appbar_widget.dart';
import 'widgets/custom_navbar_buttons_widget.dart';

import 'widgets/custom_searchbar_widget.dart';
import 'widgets/custom_taskindicator_widget.dart';
import 'widgets/gridview_options_widget.dart';

final homeProvider = SimpleProvider(
  (ref) => HomeController(),
);

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final unitHeightSize = size.height * .007;
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                children: [
                  const SafeArea(
                    child: CustomAppbar(),
                  ),
                  SizedBox(
                    height: unitHeightSize * 2.5,
                  ),
                  const CustomSearchBar(),
                  SizedBox(
                    height: unitHeightSize * 2.5,
                  ),
                  const CustomTaskIndicator(),
                  SizedBox(
                    height: unitHeightSize * 3,
                  ),
                  const Expanded(
                    child: GridViewOptions(),
                  ),
                ],
              ),
            ),
          ),
          const CustomNavBarButtons()
        ],
      ),
    );
  }
}
