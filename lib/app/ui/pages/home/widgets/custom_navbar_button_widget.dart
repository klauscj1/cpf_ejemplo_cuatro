import 'package:doctor_plus/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class NavBarButton extends StatelessWidget {
  const NavBarButton({
    Key? key,
    required this.icon,
    required this.onTap,
    required this.active,
  }) : super(key: key);

  final IconData icon;
  final VoidCallback onTap;
  final bool active;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onTap,
      child: Container(
        width: size.width * .12,
        height: size.width * .12,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: active ? AppColors.primary.withOpacity(.2) : Colors.white,
        ),
        child: Center(
          child: Icon(
            icon,
            color: active ? AppColors.primary : Colors.grey.withOpacity(.9),
            size: 28,
          ),
        ),
      ),
    );
  }
}
