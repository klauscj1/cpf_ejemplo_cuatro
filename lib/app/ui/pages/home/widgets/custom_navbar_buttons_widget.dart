import 'package:doctor_plus/app/ui/pages/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

import 'custom_navbar_button_widget.dart';

class CustomNavBarButtons extends StatelessWidget {
  const CustomNavBarButtons({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Consumer(builder: (_, ref, __) {
      final currentIndex = ref.watch(homeProvider.ids(() => ['index'])).index;
      return Container(
        color: Colors.white,
        height: size.height * .11,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            NavBarButton(
              icon: Icons.home,
              onTap: () {
                homeProvider.read.changeIndex(0);
              },
              active: currentIndex == 0,
            ),
            NavBarButton(
              icon: Icons.search_outlined,
              onTap: () {
                homeProvider.read.changeIndex(1);
              },
              active: currentIndex == 1,
            ),
            NavBarButton(
              icon: Icons.settings,
              onTap: () {
                homeProvider.read.changeIndex(2);
              },
              active: currentIndex == 2,
            ),
            NavBarButton(
              icon: Icons.note_alt_sharp,
              onTap: () {
                homeProvider.read.changeIndex(3);
              },
              active: currentIndex == 3,
            ),
            NavBarButton(
              icon: Icons.account_circle_outlined,
              onTap: () {
                homeProvider.read.changeIndex(4);
              },
              active: currentIndex == 4,
            ),
          ],
        ),
      );
    });
  }
}
