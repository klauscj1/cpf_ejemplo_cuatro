import 'package:doctor_plus/app/ui/pages/home/home_page.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';

class CustomAppbar extends StatelessWidget {
  const CustomAppbar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Row(
      children: [
        Expanded(
          child: ListTile(
            contentPadding: const EdgeInsets.symmetric(horizontal: 0),
            leading: InkWell(
              onTap: () async {
                await homeProvider.read.logout();
                Navigator.pushReplacementNamed(context, Routes.splash);
              },
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.asset(
                  'assets/images/user.png',
                  width: size.width * .1,
                ),
              ),
            ),
            title: const Text("Hi, Claus"),
            subtitle: const Text("How're you today?"),
          ),
        ),
        InkWell(
            onTap: () {},
            child: SizedBox(
              width: 70,
              height: 70,
              child: Stack(
                children: [
                  Center(
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(.5),
                              blurRadius: 15,
                            )
                          ]),
                      child: const Center(
                        child: Icon(
                          Icons.notifications_outlined,
                          color: Colors.red,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Container(
                      width: 25,
                      height: 25,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: Colors.red,
                      ),
                      child: Center(
                        child: Text(
                          "1",
                          style: Theme.of(context)
                              .textTheme
                              .caption!
                              .copyWith(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ))
      ],
    );
  }
}
