import 'package:doctor_plus/app/domain/model/option_model.dart';
import 'package:flutter/material.dart';

import 'custom_option_button_widget.dart';

class GridViewOptions extends StatelessWidget {
  const GridViewOptions({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      physics: const BouncingScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2.2,
        mainAxisSpacing: 20,
      ),
      itemCount: options.length,
      itemBuilder: (context, index) {
        return CustomOptionButton(
          icon: options[index].icon,
          onTap: () {},
          text: options[index].text,
        );
      },
    );
  }
}
