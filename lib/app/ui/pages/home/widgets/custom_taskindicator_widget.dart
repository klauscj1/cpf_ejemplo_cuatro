import 'package:doctor_plus/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CustomTaskIndicator extends StatelessWidget {
  const CustomTaskIndicator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Task for today",
                style: Theme.of(context).textTheme.headline5!.copyWith(
                      fontWeight: FontWeight.bold,
                    ),
              ),
              Text(
                "4 of 6 completed",
                style: Theme.of(context).textTheme.subtitle1!.copyWith(
                      color: Colors.grey.withOpacity(.9),
                    ),
              ),
            ],
          ),
        ),
        Container(
          width: size.width * .2,
          height: size.width * .2,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(50),
          ),
          child: Stack(
            children: [
              Center(
                child: Container(
                  width: size.width * .16,
                  height: size.width * .16,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: CircularProgressIndicator(
                    value: 50,
                    strokeWidth: 5,
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.grey.withOpacity(.6),
                    ),
                  ),
                ),
              ),
              Center(
                child: Container(
                  width: size.width * .16,
                  height: size.width * .16,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: CircularProgressIndicator(
                    value: .35,
                    strokeWidth: 6,
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Colors.green.withOpacity(.6),
                    ),
                  ),
                ),
              ),
              Center(
                child: Text(
                  "2",
                  style: Theme.of(context).textTheme.headline4!.copyWith(
                        color: AppColors.primary,
                      ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
