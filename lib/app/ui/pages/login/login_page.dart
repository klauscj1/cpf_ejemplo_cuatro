import 'package:doctor_plus/app/ui/pages/home/home_page.dart';
import 'package:doctor_plus/app/ui/pages/login/controller/login_controller.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:doctor_plus/app/ui/theme/app_colors.dart';
import 'package:doctor_plus/app/ui/widgets/custom_button.dart';
import 'package:doctor_plus/app/ui/widgets/custom_text_field.dart';
import 'package:doctor_plus/app/ui/widgets/social_networks_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

final loginProvider = SimpleProvider(
  (ref) => LoginController(),
);

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        physics: const BouncingScrollPhysics(),
        children: [
          Image.asset(
            'assets/images/isotype.png',
            height: size.height * .18,
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Welcome back!",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ],
            ),
          ),
          const Text("Email"),
          const SizedBox(
            height: 5,
          ),
          Consumer(
            builder: (_, ref, __) {
              final isValid =
                  ref.watch(loginProvider.ids(() => ['emailValid'])).emailValid;
              return CustomTextField(
                isValid: isValid,
                enableIcon: Icons.check_circle,
                disableIcon: Icons.close,
                controller: loginProvider.read.emailTextController,
                onChanged: loginProvider.read.onChangeEmail,
                onPressed: () {},
              );
            },
          ),
          const SizedBox(
            height: 15,
          ),
          const Text("Password"),
          const SizedBox(
            height: 5,
          ),
          Consumer(
            builder: (_, ref, __) {
              final showPassword = ref
                  .watch(loginProvider.ids(() => ['showPassword']))
                  .showPassword;
              return CustomTextField(
                isPassword: true,
                isValid: showPassword,
                enableIcon: Icons.visibility_off,
                disableIcon: Icons.visibility,
                controller: loginProvider.read.passwordTextController,
                onChanged: (value) {},
                onPressed: loginProvider.read.changeShowPassword,
              );
            },
          ),
          const SizedBox(
            height: 25,
          ),
          SizedBox(
            width: double.infinity,
            height: size.height * .07,
            child: CustomButton(
              label: "Log In",
              onTap: () async {
                bool ok = await loginProvider.read.initLogin();
                if (!ok) {
                  showDialog(
                    context: context,
                    builder: (_) => AlertDialog(
                      title: const Text("Error"),
                      content: Text(loginProvider.read.error),
                    ),
                  );
                } else {
                  Navigator.pushReplacementNamed(context, Routes.home);
                }
              },
            ),
          ),
          const SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(
                      context, Routes.forgerPassword);
                },
                child: Text(
                  "Forget Password?",
                  style: Theme.of(context)
                      .textTheme
                      .button!
                      .copyWith(color: AppColors.primary),
                ),
              )
            ],
          ),
          const SizedBox(
            height: 60,
          ),
          const SocialNetworksButton(),
          const SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Don't have an account, ",
                style: Theme.of(context)
                    .textTheme
                    .button!
                    .copyWith(color: AppColors.primary),
              ),
              InkWell(
                onTap: () =>
                    Navigator.pushReplacementNamed(context, Routes.register),
                child: Text(
                  "Sign Up",
                  style: Theme.of(context)
                      .textTheme
                      .button!
                      .copyWith(color: Colors.blue),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
