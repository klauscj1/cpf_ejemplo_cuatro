import 'package:doctor_plus/app/domain/repository/authentication_respository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginController extends SimpleNotifier {
  final _authenticationRepository = Get.i.find<AuthenticationRepository>();
  final _storage = Get.i.find<FlutterSecureStorage>();
  //variables
  bool _emailValid = false;
  bool _showPassword = false;
  TextEditingController emailTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  String error = "";

  //get's
  bool get emailValid => _emailValid;
  bool get showPassword => _showPassword;

  //set's
  set emailValid(bool value) {
    _emailValid = value;
    notify(['emailValid']);
  }

  set showPassword(bool value) {
    _showPassword = value;
    notify(['showPassword']);
  }
  //methods

  void onChangeEmail(String value) {
    if (value.length > 4) {
      _emailValid = true;
    } else {
      _emailValid = false;
    }
    notify(['emailValid']);
  }

  void changeShowPassword() {
    _showPassword = !_showPassword;
    notify(['showPassword']);
  }

  Future<bool> initLogin() async {
    Map<String, dynamic> response = await _authenticationRepository.login(
      email: emailTextController.text,
      password: passwordTextController.text,
    );
    if (response["token"] != null) {
      await _storage.write(key: 'token', value: response["token"]);
      return true;
    } else {
      error = response["error"];
      return false;
    }
  }
}
