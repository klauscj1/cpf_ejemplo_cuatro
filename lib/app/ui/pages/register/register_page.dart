import 'package:doctor_plus/app/domain/model/country_model.dart';
import 'package:doctor_plus/app/ui/pages/register/controller/register_controller.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:doctor_plus/app/ui/widgets/custom_back_button.dart';
import 'package:doctor_plus/app/ui/widgets/custom_button.dart';
import 'package:doctor_plus/app/ui/widgets/custom_text_field.dart';
import 'package:doctor_plus/app/ui/widgets/social_networks_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

final registerProvider = SimpleProvider(
  (ref) => RegisterController(),
);

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 40,
              ),
              CustomBackButton(
                onTap: () {
                  Navigator.pushReplacementNamed(context, Routes.onboarding);
                },
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                "Welcome to Doctor Plus",
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Colors.black),
              ),
              const SizedBox(
                height: 0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Already have an account, ",
                    style: Theme.of(context)
                        .textTheme
                        .button!
                        .copyWith(color: Colors.black),
                  ),
                  InkWell(
                    onTap: () =>
                        Navigator.pushReplacementNamed(context, Routes.login),
                    child: Text(
                      "Log In",
                      style: Theme.of(context)
                          .textTheme
                          .button!
                          .copyWith(color: Colors.blue),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              const Text("Email"),
              const SizedBox(
                height: 5,
              ),
              Consumer(
                builder: (_, ref, __) {
                  final isValid = ref
                      .watch(registerProvider.ids(() => ['emailValid']))
                      .emailValid;
                  return CustomTextField(
                    isValid: isValid,
                    enableIcon: Icons.check_circle,
                    disableIcon: Icons.close,
                    controller: registerProvider.read.emailTextController,
                    onChanged: registerProvider.read.onChangeEmail,
                    onPressed: () {},
                  );
                },
              ),
              const SizedBox(
                height: 15,
              ),
              const Text("Emergency phone"),
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(3),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Consumer(builder: (_, ref, __) {
                        final selectCountry = ref
                            .watch(
                                registerProvider.ids(() => ['selectCountry']))
                            .selectCountry;
                        return DropdownButton<int>(
                          value: selectCountry.id,
                          items: countries.map<DropdownMenuItem<int>>((e) {
                            return DropdownMenuItem(
                              child: Row(
                                children: [
                                  Image.network(e.flagUrl),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Text("+${e.codePhone}"),
                                  const SizedBox(
                                    width: 30,
                                  ),
                                ],
                              ),
                              value: e.id,
                            );
                          }).toList(),
                          onChanged: (value) =>
                              registerProvider.read.changeSelectCountry(value!),
                        );
                      }),
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  const Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        border: OutlineInputBorder(),
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 15,
              ),
              const Text("Password"),
              const SizedBox(
                height: 5,
              ),
              Consumer(
                builder: (_, ref, __) {
                  final showPassword = ref
                      .watch(registerProvider.ids(() => ['showPassword']))
                      .showPassword;
                  return CustomTextField(
                    isPassword: true,
                    isValid: showPassword,
                    enableIcon: Icons.visibility_off,
                    disableIcon: Icons.visibility,
                    controller: registerProvider.read.passwordTextController,
                    onChanged: (value) {},
                    onPressed: registerProvider.read.changeShowPassword,
                  );
                },
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: size.height * .07,
                child: CustomButton(
                    label: "Sign Up",
                    onTap: () async {
                      bool ok = await registerProvider.read.initRegister();
                      if (!ok) {
                        showDialog(
                          context: context,
                          builder: (_) => AlertDialog(
                            title: const Text("Error"),
                            content: Text(registerProvider.read.error),
                          ),
                        );
                      }
                    }),
              ),
              const SizedBox(
                height: 20,
              ),
              const SocialNetworksButton(),
              const SizedBox(
                height: 15,
              ),
              const Center(
                child: Text("By clicking sign up you are agreeting to the"),
              ),
              const SizedBox(
                height: 5,
              ),
              const Center(
                child: Text(
                  "Term of use and the Private Policy",
                  style: TextStyle(color: Colors.blue),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
