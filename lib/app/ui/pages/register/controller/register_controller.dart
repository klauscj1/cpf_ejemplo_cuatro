import 'package:doctor_plus/app/domain/model/country_model.dart';
import 'package:doctor_plus/app/domain/repository/authentication_respository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

class RegisterController extends SimpleNotifier {
  final _authenticationRepository = Get.i.find<AuthenticationRepository>();
  //variables
  bool _emailValid = false;
  bool _showPassword = false;
  TextEditingController emailTextController = TextEditingController();
  TextEditingController passwordTextController = TextEditingController();
  Country _selectCountry = Country(
    name: 'Ecuador',
    flagUrl: 'https://flagcdn.com/w20/ec.png',
    code: 'EC',
    id: 1,
    codePhone: 593,
  );
  String error = "";

  //get's
  bool get emailValid => _emailValid;
  bool get showPassword => _showPassword;
  Country get selectCountry => _selectCountry;

  //set's
  set emailValid(bool value) {
    _emailValid = value;
    notify(['emailValid']);
  }

  set showPassword(bool value) {
    _showPassword = value;
    notify(['showPassword']);
  }

  set selectCountry(Country value) {
    _selectCountry = value;
    notify(['selectCountry']);
  }
  //methods

  void onChangeEmail(String value) {
    if (value.length > 4) {
      _emailValid = true;
    } else {
      _emailValid = false;
    }
    notify(['emailValid']);
  }

  void changeShowPassword() {
    _showPassword = !_showPassword;
    notify(['showPassword']);
  }

  void changeSelectCountry(int value) {
    Country? countryExist;
    for (Country country in countries) {
      if (country.id == value) {
        countryExist = country;
        break;
      }
    }
    _selectCountry = countryExist!;
    notify(['selectCountry']);
  }

  Future<bool> initRegister() async {
    Map<String, dynamic> response = await _authenticationRepository.register(
      email: emailTextController.text,
      password: passwordTextController.text,
    );
    if (response["token"] != null) {
      return true;
    } else {
      error = response["error"];
      return false;
    }
  }
}
