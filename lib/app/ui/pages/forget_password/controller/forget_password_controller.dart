import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

class ForgetPasswordController extends SimpleNotifier {
  //variables
  bool _emailValid = false;

  TextEditingController emailTextController = TextEditingController();

  //get's
  bool get emailValid => _emailValid;

  //set's
  set emailValid(bool value) {
    _emailValid = value;
    notify(['emailValid']);
  }

  //methods

  void onChangeEmail(String value) {
    if (value.length > 4) {
      _emailValid = true;
    } else {
      _emailValid = false;
    }
    notify(['emailValid']);
  }
}
