import 'package:doctor_plus/app/ui/pages/forget_password/controller/forget_password_controller.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:doctor_plus/app/ui/widgets/custom_back_button.dart';
import 'package:doctor_plus/app/ui/widgets/custom_button.dart';
import 'package:doctor_plus/app/ui/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';
import 'package:flutter_meedu/meedu.dart';

final forgetPasswordProvider = SimpleProvider(
  (ref) => ForgetPasswordController(),
);

class ForgetPasswordPage extends StatelessWidget {
  const ForgetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              CustomBackButton(
                onTap: () {
                  Navigator.pushReplacementNamed(context, Routes.login);
                },
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                "Forget Password",
                style: Theme.of(context)
                    .textTheme
                    .headline4!
                    .copyWith(color: Colors.black),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "Please enter your email below to receive your\npassword reset instructions",
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                    ),
              ),
              const SizedBox(
                height: 30,
              ),
              const Text("Email"),
              const SizedBox(
                height: 5,
              ),
              Consumer(
                builder: (_, ref, __) {
                  final isValid = ref
                      .watch(forgetPasswordProvider.ids(() => ['emailValid']))
                      .emailValid;
                  return CustomTextField(
                    isValid: isValid,
                    enableIcon: Icons.check_circle,
                    disableIcon: Icons.close,
                    controller: forgetPasswordProvider.read.emailTextController,
                    onChanged: forgetPasswordProvider.read.onChangeEmail,
                    onPressed: () {},
                  );
                },
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * .07,
                child: CustomButton(
                  label: "Send Email",
                  onTap: () {
                    Navigator.pushNamed(context, Routes.recoveryPassword);
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
