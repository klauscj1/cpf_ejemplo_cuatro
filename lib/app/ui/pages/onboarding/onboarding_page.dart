import 'package:doctor_plus/app/ui/pages/onboarding/controller/onboarding_controller.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:doctor_plus/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

import 'widgets/dot_container.dart';
import 'widgets/image_card_widget.dart';

final onboardingProvider = SimpleProvider(
  (ref) => OnboardingController(),
);

class OnboardingPage extends StatelessWidget {
  const OnboardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          //BACKGROUND
          Container(
            color: AppColors.primary,
          ),
          //WIDGETS
          Column(
            children: [
              SizedBox(
                //IMAGE CONTAINER
                width: double.infinity,
                height: size.height * .65,
                child: Consumer(builder: (_, ref, __) {
                  final items = ref.watch(onboardingProvider).items;
                  return PageView.builder(
                    controller: onboardingProvider.read.pageController,
                    physics: const BouncingScrollPhysics(),
                    itemCount: items.length,
                    itemBuilder: (_, index) {
                      return ImageCardWidget(
                        index: index,
                        item: items[index],
                        listItemSize: items.length,
                      );
                    },
                  );
                }),
              ),
              SizedBox(
                //BUTTONS CONTAINER
                width: double.infinity,
                height: size.height * .35,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const DotContainer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          width: size.width * .4,
                          height: 45,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              side: const BorderSide(
                                  width: 1.0, color: Colors.white),
                            ),
                            onPressed: () {
                              Navigator.pushReplacementNamed(
                                  context, Routes.login);
                            },
                            child: Text(
                              "Login",
                              style: Theme.of(context)
                                  .textTheme
                                  .button!
                                  .copyWith(color: Colors.white),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: size.width * .4,
                          height: 45,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.white,
                            ),
                            onPressed: () => Navigator.pushReplacementNamed(
                                context, Routes.register),
                            child: Text(
                              "Sign up",
                              style: Theme.of(context)
                                  .textTheme
                                  .button!
                                  .copyWith(color: AppColors.primary),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      "Are you a doctor? Get here!",
                      style: Theme.of(context)
                          .textTheme
                          .button!
                          .copyWith(color: Colors.white),
                    )
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
