import 'package:doctor_plus/app/domain/model/car_item_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

class OnboardingController extends SimpleNotifier {
  OnboardingController() {
    pageController.addListener(updateIndexSelected);
  }
  updateIndexSelected() {
    _indexSelected = (pageController.page! + 0.5).toInt();
    notify(['indexSelected']);
  }

  //variables
  int _indexSelected = 0;
  PageController pageController = PageController(initialPage: 0);
  List<CardItem> items = [
    CardItem(
      id: 1,
      title: "Access thousands of trusted Doctor instanly",
      imagePath: 'assets/images/onb1.jpg',
    ),
    CardItem(
      id: 2,
      title: "Access thousands of trusted Doctor instanly",
      imagePath: 'assets/images/onb2.jpg',
    ),
    CardItem(
      id: 3,
      title: "Access thousands of trusted Doctor instanly",
      imagePath: 'assets/images/onb3.jpg',
    ),
    CardItem(
      id: 4,
      title: "Access thousands of trusted Doctor instanly",
      imagePath: 'assets/images/onb4.jpg',
    ),
    CardItem(
      id: 5,
      title: " 1Access thousands of trusted Doctor instanly",
      imagePath: 'assets/images/onb1.jpg',
    ),
  ];

  //get's
  int get indexSelected => _indexSelected;

  //set's
  set indexSelected(int value) {
    _indexSelected = value;
    notify(['indexSelected']);
  }

  @override
  void dispose() {
    pageController.removeListener(updateIndexSelected);
    super.dispose();
  }
}
