import 'package:flutter/material.dart';
import 'package:flutter_meedu/state.dart';

import '../onboarding_page.dart';
import 'dot_widget.dart';

class DotContainer extends StatelessWidget {
  const DotContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (_, ref, __) {
        final items = onboardingProvider.read.items;
        final indexSelected = ref
            .watch(onboardingProvider.ids(() => ['indexSelected']))
            .indexSelected;

        List<Widget> children = [];
        for (int i = 0; i < items.length; i++) {
          children.add(Row(
            children: [
              Dot(
                active: i == indexSelected,
              ),
              SizedBox(
                width: i != (items.length - 1) ? 20 : 0,
              ),
            ],
          ));
        }
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: children,
        );
      },
    );
  }
}
