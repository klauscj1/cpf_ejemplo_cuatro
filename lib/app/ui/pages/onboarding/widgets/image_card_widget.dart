import 'package:doctor_plus/app/domain/model/car_item_model.dart';
import 'package:flutter/material.dart';

class ImageCardWidget extends StatelessWidget {
  const ImageCardWidget({
    Key? key,
    required this.item,
    required this.index,
    required this.listItemSize,
  }) : super(key: key);

  final CardItem item;
  final int index;
  final int listItemSize;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.only(
        bottomLeft: Radius.circular(index == 0 ? 50 : 0),
        bottomRight: Radius.circular(index == (listItemSize - 1) ? 50 : 0),
      ),
      child: Stack(
        children: [
          Container(
            height: double.infinity,
            color: Colors.white,
            child: Image.asset(
              item.imagePath!,
              fit: BoxFit.fitHeight,
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              height: MediaQuery.of(context).size.height * .6 * .3,
              decoration: BoxDecoration(
                color: Colors.white,
                gradient: LinearGradient(
                  colors: [
                    Colors.white.withOpacity(.05),
                    Colors.white.withOpacity(1),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: const [.05, .4],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12),
                    child: Text(
                      item.title!,
                      style: Theme.of(context).textTheme.headline6!.copyWith(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                  ),
                  const SizedBox(height: 40),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
