import 'package:flutter/material.dart';

class Dot extends StatelessWidget {
  const Dot({
    Key? key,
    required this.active,
  }) : super(key: key);

  final bool active;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final sizeSmall = size.width * .010;
    final sizeLarge = size.width * .020;
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(100),
      ),
      width: active ? sizeLarge : sizeSmall,
      height: active ? sizeLarge : sizeSmall,
    );
  }
}
