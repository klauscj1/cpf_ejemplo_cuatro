import 'package:doctor_plus/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class SocialNetworksButton extends StatelessWidget {
  const SocialNetworksButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Log in with social account",
              style: Theme.of(context)
                  .textTheme
                  .button!
                  .copyWith(color: AppColors.primary),
            )
          ],
        ),
        const SizedBox(
          height: 30,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              width: size.width * .4,
              height: 45,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  side: const BorderSide(
                    width: 0,
                    color: Color(0Xff4267B2),
                  ),
                ),
                onPressed: () {},
                child: Text(
                  "Facebook",
                  style: Theme.of(context)
                      .textTheme
                      .button!
                      .copyWith(color: Colors.white),
                ),
              ),
            ),
            SizedBox(
              width: size.width * .4,
              height: 45,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  primary: Colors.blue[200],
                ),
                onPressed: () {},
                child: Text(
                  "Twitter",
                  style: Theme.of(context)
                      .textTheme
                      .button!
                      .copyWith(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
