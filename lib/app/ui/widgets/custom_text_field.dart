import 'package:doctor_plus/app/ui/theme/app_colors.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  const CustomTextField({
    Key? key,
    this.isPassword = false,
    required this.isValid,
    required this.enableIcon,
    required this.disableIcon,
    required this.controller,
    required this.onChanged,
    required this.onPressed,
  }) : super(key: key);

  final bool isPassword;
  final bool isValid;
  final IconData enableIcon;
  final IconData disableIcon;
  final TextEditingController controller;
  final Function(String) onChanged;
  final VoidCallback onPressed;

  final colorSuccess = AppColors.primary;
  final colorError = Colors.red;

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      controller: controller,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.white,
        contentPadding:
            const EdgeInsets.symmetric(vertical: 17, horizontal: 10),
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.grey,
            width: 1,
          ),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: AppColors.primary,
            width: 1,
          ),
        ),
        suffixIcon: isPassword
            ? IconButton(
                onPressed: onPressed,
                icon: Icon(
                  isValid ? enableIcon : disableIcon,
                  color: isValid ? colorError : colorSuccess,
                ),
              )
            : Icon(
                isValid ? enableIcon : disableIcon,
                color: isValid ? colorSuccess : colorError,
              ),
      ),
      obscureText: isPassword ? !isValid : false,
    );
  }
}
