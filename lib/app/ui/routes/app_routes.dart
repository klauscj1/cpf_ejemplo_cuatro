import 'package:doctor_plus/app/ui/pages/forget_password/forget_password.dart';
import 'package:doctor_plus/app/ui/pages/home/home_page.dart';
import 'package:doctor_plus/app/ui/pages/login/login_page.dart';
import 'package:doctor_plus/app/ui/pages/onboarding/onboarding_page.dart';
import 'package:doctor_plus/app/ui/pages/recovery_password/recovery_password_page.dart';
import 'package:doctor_plus/app/ui/pages/register/register_page.dart';
import 'package:doctor_plus/app/ui/pages/splash/splash_page.dart';
import 'package:doctor_plus/app/ui/routes/routes.dart';
import 'package:flutter/material.dart';

Map<String, Widget Function(BuildContext)> routes = {
  Routes.splash: (_) => const SplashPage(),
  Routes.onboarding: (_) => const OnboardingPage(),
  Routes.login: (_) => const LoginPage(),
  Routes.forgerPassword: (_) => const ForgetPasswordPage(),
  Routes.recoveryPassword: (_) => const RecoveryPasswordPage(),
  Routes.register: (_) => const RegisterPage(),
  Routes.home: (_) => const HomePage(),
};
