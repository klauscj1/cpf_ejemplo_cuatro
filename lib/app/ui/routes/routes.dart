abstract class Routes {
  static const splash = 'splash';
  static const onboarding = 'onboarding';
  static const login = 'login';
  static const register = 'register';
  static const forgerPassword = 'forgerPassword';
  static const recoveryPassword = 'recoveryPassword';
  static const home = 'home';
}
