import 'package:doctor_plus/app/data/datasource/remote/authentication_api.dart';
import 'package:doctor_plus/app/domain/repository/authentication_respository.dart';

class AuthenticationRepositoryImpl extends AuthenticationRepository {
  final AuthenticationAPI _api;
  AuthenticationRepositoryImpl(this._api);
  @override
  Future<Map<String, dynamic>> login(
          {required String email, required String password}) =>
      _api.login(email, password);

  @override
  Future<Map<String, dynamic>> register(
          {required String email, required String password}) =>
      _api.register(email, password);
}
