import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_meedu/flutter_meedu.dart';

class AuthenticationAPI {
  final _dio = Get.i.find<Dio>();

  Future<Map<String, dynamic>> login(String email, String password) async {
    Map<String, dynamic> dev = {};
    print('llega -> $email - $password');
    try {
      final response = await _dio.post(
        'auth/login',
        data: {
          'email': email,
          'password': password,
          'password_encriptado':
              "\$2a\$10\$9obV/yV/2eligMMut1/PEeNDxrpYur9GaQHD7okMQr4rud.n/Xnee"
        },
        options: Options(
          sendTimeout: 3000,
          receiveTimeout: 5000,
        ),
      );

      print(response);

      dev = {
        'error': null,
        'token': response.data['token'],
        'msg': null,
      };
      return dev;
    } on DioError catch (e) {
      print(e.response);
      String messages = "";
      final errors =
          json.decode(e.response.toString())["errors"] as Map<String, dynamic>;
      errors.forEach((key, value) {
        if (messages.isEmpty) {
          messages += "- " + value["msg"];
        } else {
          messages += "\n- " + value["msg"];
        }
      });
      dev = {
        'error': messages,
        'token': null,
      };
      return dev;
    } on Exception catch (e) {
      dev = {
        'error': e.toString(),
        'token': null,
      };
      return dev;
    }
  }

  Future<Map<String, dynamic>> register(String email, String password) async {
    Map<String, dynamic> dev = {};
    print('llega -> $email - $password');
    try {
      final response = await _dio.post('auth', data: {
        'email': email,
        'password': password,
      });

      dev = {
        'error': null,
        'token': response.data['token'],
        'msg': null,
      };
      return dev;
    } on DioError catch (e) {
      String messages = "";
      final errors =
          json.decode(e.response.toString())["errors"] as Map<String, dynamic>;
      errors.forEach((key, value) {
        if (messages.isEmpty) {
          messages += "- " + value["msg"];
        } else {
          messages += "\n- " + value["msg"];
        }
      });
      dev = {
        'error': messages,
        'token': null,
      };
      return dev;
    } on Exception catch (e) {
      dev = {
        'error': e.toString(),
        'token': null,
      };
      return dev;
    }
  }
}
